# -*- coding: utf-8 -*-

from TSPsolver import *
from easy_TSP_solver import *

path = '/windows/D/GEO/TSP/point_to_point_TSP/'
f_path = path + 'check_points.shp'

G = nx.read_shp(f_path)

GD = easCreateTINgraph(G, id_field = 'cp_id')

s_path = TSPsolver(GD, 10000, '0', '34')
#s_path = finalisation(s_path, 6000)
print s_path[0], s_path[1], s_path[2]

#for edge in s_path[3].edges(data = True):
  #if edge[2]['Delaunay']:
    #print s_path[3][edge[0]][edge[1]]['TSP_weight']['current_weight'], '|', s_path[3][edge[0]][edge[1]]['TSP_weight']['default_weight']

route = s_path[0]
showGraph(GD, route, label = 'cp_id')

#GD = createTINgraph(G, id_field = 'cp_id')

#s_path = hardTSPsolver(GD, 10000, '0', '34')
#route = s_path[0]
#f_path = finalisation(s_path, 6000, TSP = False)
#route1 = f_path[0]
#print s_path[0], s_path[1], s_path[2]
#print f_path[0], f_path[1], f_path[2], f_path[4]
#showGraph(GD, route, label = 'cp_id')
#showGraph(GD, route1, label = 'cp_id')