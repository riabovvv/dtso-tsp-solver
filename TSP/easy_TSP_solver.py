# -*- coding: utf-8 -*-
"""
Created on Fri Oct 31 18:11:15 2014

@author: Yury V. Ryabov, riabovvv@gmail.com
"""
#from TSPsolver import  showGraph#, routeCost, localOptimisation
import networkx as nx
import scipy.spatial
import matplotlib.pyplot as plt
import sys
import random
from math import sqrt


def easCreateTINgraph(point_graph, id_field = 'id', show = False, calculate_distance = True):
  '''
  Creates a graph based on Delaney triangulation

  @param point_graph: either a graph made by read_shp() from another NetworkX's point graph
  @param show: whether or not resulting graph should be shown, boolean
  @param calculate_distance: whether length of edges should be calculated
  @return - a graph made from a Delauney triangulation

  @Copyright notice: this code is an improved (by Yury V. Ryabov, 2014, riabovvv@gmail.com) version of
                    Tom's code taken from this discussion
                    https://groups.google.com/forum/#!topic/networkx-discuss/D7fMmuzVBAw
  '''

  TIN = scipy.spatial.Delaunay(point_graph)
  edges = set()
  # for each Delaunay triangle
  for n in xrange(TIN.nsimplex):
      # for each edge of the triangle
      # sort the vertices
      # (sorting avoids duplicated edges being added to the set)
      # and add to the edges set
      edge = sorted([TIN.vertices[n,0], TIN.vertices[n,1]])
      edges.add((edge[0], edge[1]))
      edge = sorted([TIN.vertices[n,0], TIN.vertices[n,2]])
      edges.add((edge[0], edge[1]))
      edge = sorted([TIN.vertices[n,1], TIN.vertices[n,2]])
      edges.add((edge[0], edge[1]))


  # make a graph based on the Delaunay triangulation edges
  graph = nx.Graph(list(edges))

  # add nodes attributes to the TIN graph from the original points
  original_nodes = point_graph.nodes(data = True)
  for n in xrange(len(original_nodes)):
    XY = original_nodes[n][0] # X and Y tuple - coordinates of the original points
    graph.node[n]['XY'] = XY
    # add other attributes
    original_attributes = original_nodes[n][1]
    for i in original_attributes.iteritems(): # for tuple i = (key, value)
      graph.node[n][i[0]] = i[1]
  # relabel nodes
  remap = {}
  for node, attribute in graph.nodes(data = True):
    new_name = attribute[id_field]
    remap[node] = new_name
  graph = nx.relabel_nodes(graph, remap)


  # calculate Euclidian length of edges and write it as edges attribute
  if calculate_distance:
    edges = graph.edges()
    for i in xrange(len(edges)):
      edge = edges[i]
      node_1 = edge[0]
      node_2 = edge[1]
      x1, y1 = graph.node[node_1]['XY']
      x2, y2 = graph.node[node_2]['XY']
      dist = sqrt( pow( (x2 - x1), 2 ) + pow( (y2 - y1), 2 ) )
      dist = round(dist, 2)
      graph.edge[node_1][node_2]['distance'] = dist


  # plot graph
  if show:
    showGraph(graph)

  graph = graph.to_directed()
  return graph





def easAssignWeights(graph, route, route_cost, success):
  '''
  Assigns weights to the edges of the graph along the given route depending on whether the route's successfulness

  Parameters
  ----------

  graph : a NetworkX graph

  route : a route along the graph, a list of points

  cost : cost of the route, numerical

  success : indicates whether route connected all the points from start to end or
              ended up in a dead end, Boolean


  Returns
  -------

  True if executed successfully
  '''
  if isinstance(success, bool):
    pass
  else:
    sys.exit("Parameter 'success' is not Boolean")

  steps = len(route) - 1 # number of edges in the route (not a parameter 'route')
  try:
    delta_weight = steps / route_cost
  except:
    print 'route:', route, '|', 'cost:', route_cost
  for step in xrange(steps):
    node1 = route[step]
    node2 = route[step + 1]
    d_step = step + 1
    step_key = 'step_%s' %(d_step)
    default_weight = graph[node1][node2]['TSP_weight']['default_weight']
    try:
      current_weight = graph[node1][node2]['TSP_weight'][step_key]['current_weight']
    except:
      current_weight = default_weight + delta_weight
    if success and current_weight < default_weight:
      current_weight = default_weight + delta_weight
      graph[node1][node2]['TSP_weight'][step_key]['successes'] += 1
    elif success: # shortest route have been found
#      print graph[node1][node2], 'step:', step_key
      current_weight += delta_weight
      try:
        graph[node1][node2]['TSP_weight'][step_key]['successes'] += 1
      except:
        graph[node1][node2]['TSP_weight'][step_key] = {}
        graph[node1][node2]['TSP_weight'][step_key]['successes'] = 0
        graph[node1][node2]['TSP_weight'][step_key]['successes'] += 1
    else: # a route leads to a deadend
      current_weight += delta_weight
#      print graph[node1][node2]
#      print 'step key:', step_key
      try:
        graph[node1][node2]['TSP_weight'][step_key]['dead_ends'] += 1
      except:
        graph[node1][node2]['TSP_weight'][step_key] = {}
        graph[node1][node2]['TSP_weight'][step_key]['dead_ends'] = 0
        graph[node1][node2]['TSP_weight'][step_key]['dead_ends'] += 1

  if current_weight < 0: # we don't want either negative or zero weights
    current_weight = 0.00000001
  graph[node1][node2]['TSP_weight'][step_key]['current_weight'] = current_weight
  return True


def easChooseNextNode(nodes_dict):
  '''
  Randomly chooses next poont to move taking into account weights of the paths

  Parameters
  ..........

  nodes_dict:
                a dictionary that contains nodes as keys; values - dictionary
                with weights for the edge to the current node {node: weight}


  Returns
  .......

  A note to move to.
  '''
  # assign probabilities
  total_weight = 0 # sum of weights will be "1" (sum of probabilities)
  weight_dict = {} # {node: weght}
  weight_difference = {} # {node: delta_weight}
  to_correct = 0 # number of nodes to correct weights
  for weight in nodes_dict.itervalues(): # culculate total weight of nodes
    total_weight += weight
  for node, weight in nodes_dict.iteritems(): # calculate probabilities to choose given node
    new_weight = float(weight) / total_weight
    new_weight = round(new_weight, 2)
    weight_dict[node] = new_weight
    if new_weight < 0.05: # we don't want probabilities to bee too smal
      weight_difference[node] = 0.05 - weight
      to_correct += 1
  if to_correct > 0: # recalculate weights
    total_difference = 0
    for value in weight_difference.itervalues():
      total_difference += value
    donation = total_difference
    for node, weight in weight_dict.iteritems():
      sponsor = max(weight_dict.itervalues()) # a value of node to subtract values from
      if weight == sponsor:
        sponsor = node # a node to subtract values from
        break
    weight_dict[sponsor] -= donation
    for node in weight_difference.iterkeys(): # correct probabilities
      delta = weight_difference[node]
      weight_dict[node] += delta
  #choose the next node
  ranges = {} # {(range_start, range_end): node}
  range_start = 0
  range_end = 0
  for node, weight in weight_dict.iteritems():
#    print node, weight
#    if range_start == 0:
    range_end = range_start + weight
    ranges[(range_start, range_end)] = node
    range_start = range_end
#    else:
#      range_end = range_start + weight
#      ranges[(range_start, range_end)] = node
#      range_start += range_end
  chance = float(random.randrange(0, 100)) / 100
  for ran in ranges.iterkeys():
    range_start = round(ran[0], 2)
    range_end = round(ran[1], 2)
    if 99 > range_end >= 0.98:
      range_end = 0.99
    if range_start <= chance <= range_end:
      next_node = ranges[ran]
#      print 'next node:', next_node
      break
  # unfortunately in rare cases chance is out of range of any given ranges (usually when chance = 0.99). Fix it this way
  try:
    next_node = next_node
  except:
    print 'ahtung! "randes" assignment failed!'
    print 'chance:', chance
    for ran in ranges.iterkeys():
      print ran
    n_nodes = len(ranges)
    lucky = random.randrange(n_nodes)
    i = 0
    for node in ranges.itervalues():
      if i == lucky:
        next_node = node
      else:
        i += 1

  return next_node


def routeCost(graph, route, cost_attribute):
  '''
  Calculates cost of the route on the given graph.

  Parameters
  ----------

  graph: NetworkX graph

  route: sequence of points on the graph, list

  cost_attribute: edges attribute with cost walues, string

  Returns
  -------

  Cost of the given route
  '''
  cost = 0
  for i in xrange(len(route) - 1):
    node1 = route[i]
    node2 = route[i + 1]
    try:
      edge_cost = graph[node1][node2][cost_attribute]
    except:
      print 'routeCost(): invalid edge in route:', (node1, node2)
      print route, len(route)
      showGraph(graph, route, label = 'id', all_edges = 'all')
      sys.exit('routeCost() failed')
    else:
      cost += edge_cost

  return cost


def TSPsolver(graph, path_finders, start_node, end_node = None, cost = 'distance'):
  '''
  Resolves (hopefully ;-) Travelling Salesman Problem at the given graph using 'smart path-finders' algorithm

  Parameters
  ----------

  graph:
    a NetworkX craph to resolve TSP with.

  path_finders:
    number of 'smart path-finders' to deploy to find the route, integer.

  start_node:
    a note to start route from.

  end_node:
    a node to end route, if not specified a starting node is presumed to be end node

  cost:
    an attribute of the graph edges to derive cost of travel from 'distance' is the default value

  Returns
  -------

  A NetworkX graph which is the shortest path (hopefully ;-) from the start node
  to the end node that visits all other nodes once, lengt of the route
  '''


  # assign weights to edges
  all_edges = graph.edges()
  for i in xrange(len(all_edges)):
    n1, n2 = all_edges[i]
    default_weight = graph[n1][n2][cost]
    graph[n1][n2]['TSP_weight'] = {'default_weight': None} # will contain weights for each step (step number - key, step number weigth - value)
    # what if user passed some bullshit as a weight???
    try:
      graph[n1][n2]['TSP_weight']['default_weight'] = 1 / default_weight
    except:
      if float(default_weight) == 0.0:
        graph[n1][n2]['TSP_weight']['default_weight'] = 1
      else:
        graph[n1][n2]['TSP_weight']['default_weight'] = 1 / float(default_weight)
    else:
      if not isinstance(default_weight, float) and not isinstance(default_weight, int):
        message = 'ERROR: "%s" attribute of the edge (%d, %d) has type "%s", but it must be either "float" or "integer".\nExecution aborted!' %(cost, n1, n2, type(default_weight))
        sys.exit(message)

  # define end_node
  if not end_node:
    end_node = start_node
    symmetric_TSP = True
  else:
    symmetric_TSP = False

  # start path finding
  if symmetric_TSP:
    total_nodes = len(graph.nodes()) + 1
  else:
    total_nodes = len(graph.nodes())
  shortest_route = None
  shortest_route_length = None
  successful_iteration = None
  iteration = 0
#  fail_routes = []
  for i in xrange(path_finders): # launch N path-finders
    iteration += 1
#    print path_finders - i, 'to go'
    nodes_to_go = total_nodes
    end_node_reached = False
    can_move = True
    route = [] # nodes visited during the path-finding
    current_node = start_node
    step = 0 # number of the current step

    while not end_node_reached and can_move:
      step += 1
      step_key = 'step_%d' %(step)
      route.append(current_node)
      nodes_to_go -= 1
      neighbors = graph.neighbors(current_node)
      to_visit = {} # unvisited nodes amongst current neighbors {node: current_weight}
      to_visit_def = {} # unvisited nodes amongst current neighbors {node: default_weight}
      enough_data = True # indicates that there is exist statistics for the current step for each of the egdes to the neighbors

      for i in xrange(len(neighbors)):
        node = neighbors[i]
#        if not symmetric_TSP or (symmetric_TSP and nodes_to_go > 1):
        if node not in route or (symmetric_TSP and nodes_to_go == 1 and node == end_node):
          #if graph[current_node][node]['Delaunay']:
            to_visit_def[node] = graph[current_node][node]['TSP_weight']['default_weight']
            try:
              # add statistics for this edge for current step to the to_visit dict
              to_visit[node] = graph[current_node][node]['TSP_weight'][step_key]['current_weight']
            except:
              # this means that for one or more edges there is no statistis for the current step
              enough_data = False
              to_visit[node] = graph[current_node][node]['TSP_weight']['default_weight']
              # add statistics pattern to edge
              graph[current_node][node]['TSP_weight'][step_key] = {}
              graph[current_node][node]['TSP_weight'][step_key]['successes'] = 0
              graph[current_node][node]['TSP_weight'][step_key]['dead_ends'] = 0
              default_weight = graph[current_node][node]['TSP_weight']['default_weight']
              graph[current_node][node]['TSP_weight'][step_key]['current_weight'] = default_weight
        else:
          continue

      # define conditions for unsuccessful route
      n_ways = len(to_visit)
      if n_ways == 0: # dead end
        can_move = False
        route_length = routeCost(graph, route, cost)
        easAssignWeights(graph, route, route_length, False)
      # if route goes on need to choose next node
      else:
        if n_ways == 1: # there is only one node to go
          current_node = to_visit.keys()[0]
          if current_node == end_node and nodes_to_go == 1: # route completed
            end_node_reached = True
            route.append(current_node)
            #route = localOptimisation(graph, route, cost)
            route_length = routeCost(graph, route, cost)
            if shortest_route_length > route_length or shortest_route_length is None:
              shortest_route_length = route_length
              shortest_route = route
              easAssignWeights(graph, route, route_length, True)
              successful_iteration = iteration
              print 'route found!', successful_iteration
            else:
              continue
          elif current_node == end_node and nodes_to_go > 1: # can go only to the end_node, buth some other nodes left to visit
            can_move = False
            route_length = routeCost(graph, route, cost)
#            easAssignWeights(graph, route, route_length, False)
          else: # route incomplete
            continue
        else:
          # check if there is end_node in canditates for the next move
          if end_node in to_visit.keys():
            del to_visit[end_node]
          if len(to_visit) == 1:
            current_node = to_visit.keys()[0]
          else:
            if enough_data:
              current_node = easChooseNextNode(to_visit)
            else:
              current_node = easChooseNextNode(to_visit_def)

#    if route not in fail_routes:
#      fail_routes.append(route)
#  for i in fail_routes:
#    print i
  return shortest_route, shortest_route_length, successful_iteration




def showGraph(graph, route = None, label = None, all_edges = None, pos_dict = None, file_path = None):

  if all_edges not in [None, 'all', 'Delaunay']:
    sys.exit('showGraph(): invalid "all_edges" parameter "%s"') %(all_edges)
  if pos_dict == None:
    pos_dict = {}
    nodes = graph.nodes(data = True)
    for node, data in nodes:
      position = data['XY']
      pos_dict[node] = position
  else:
    pass

  if all_edges == 'all':
    un_graph = graph.to_undirected() # make graph undirected
    nx.draw(un_graph, pos_dict)
  elif all_edges == 'Delaunay':
    un_graph = graph.to_undirected() # make graph undirected
    edge_list = []
    for edge in graph.edges(data = True): # 'edge' looks like this: (48, 47, {'distance': 2531.03, 'Delaunay': False})
      if edge[2]['Delaunay']:
        edge_list.append( (edge[0], edge[1]) )
    nx.draw_networkx_edges(un_graph, pos_dict, edgelist = edge_list)
  # draw nodes
  nx.draw_networkx_nodes(graph, pos = pos_dict, node_size = 25)

  # draw edges
  if route:
    edge_list = []
    for i in xrange(len(route) - 1):
      start_node = route[i]
      end_node = route[i + 1]
      edge_list.append((start_node, end_node))
    nx.draw_networkx_edges(graph, edgelist = edge_list, pos = pos_dict, edge_color = 'b', width = 3.0)

  # draw labels
  if label:
    labs = {}
    for node in route:
      text = graph.node[node][label]
      labs[node] = text
    nx.draw_networkx_labels(graph, pos_dict, labels = labs, font_size = 12)
  if file_path:
    plt.savefig(file_path, dpi = 300)
    plt.clf()
  else:
    plt.show()