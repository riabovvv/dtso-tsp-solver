# -*- coding: utf-8 -*-

from TSPsolver import *
#from easy_TSP_solver import finalisation#, TSPsolver, easAssignWeights, easChooseNextNode
import timeit
import time
import re

#path = '/windows/D/GEO/TSP/symmetrical_TSP/att48/'
#f_path = path + 'att48.shp'

path = '/windows/D/GEO/TSP/symmetrical_TSP/eil76/'
f_path = path + 'eil76.shp'

#path = '/windows/D/GEO/TSP/symmetrical_TSP/att532/'
#f_path = path + 'att532.shp'

G = nx.read_shp(f_path)

GD = createTINgraph(G, TSPLIB_mode=True, pseudo_eucledian=False)#, id_field = 'cp_id')
#showGraph(GD, all_edges = True)

#cut_off_route = 1,33,63,16,3,44,32,9,39,72,58,10,31,55,25,50,18,24,49,23,56,41,43,42,64,22,61,21,47,36,69,71,60,70,20,37,5,15,57,13,54,19,14,59,66,65,38,11,53,7,35,8,46,34,52,27,45,29,48,30,4,75,76,67,26,12,40,17,51,6,68,2,74,28,62,73,1
#cut_off_route = [76, 75, 4, 68, 6, 51, 17, 26, 67, 34, 46, 52, 27, 45, 29, 5, 37, 47, 48, 30, 2, 62, 73, 1, 43, 41, 56, 23, 49, 24, 18, 50, 25, 55, 31, 9, 39, 72, 58, 10, 38, 65, 66, 11, 59, 14, 53, 7, 35, 8, 19, 54, 13, 57, 15, 20, 70, 60, 71, 36, 69, 61, 21, 74, 28, 22, 64, 42, 33, 63, 16, 44, 3, 32, 40, 12, 76]
#cut_off_route = [76, 75, 68, 2, 33, 73, 62, 22, 28, 61, 21, 74, 30, 48, 47, 36, 69, 71, 60, 70, 20, 37, 5, 15, 57, 13, 54, 19, 35, 7, 26, 12, 40, 17, 3, 44, 32, 9, 39, 72, 58, 53, 14, 59, 11, 66, 65, 38, 10, 31, 55, 25, 50, 18, 24, 49, 23, 56, 41, 64, 42, 43, 1, 63, 16, 51, 6, 4, 45, 29, 27, 52, 8, 46, 34, 67, 76]
#cut_off_route = [13, 57, 15, 27, 45, 29, 5, 37, 20, 70, 60, 71, 36, 69, 61, 21, 47, 48, 74, 28, 22, 64, 42, 41, 56, 23, 49, 24, 18, 50, 32, 44, 3, 16, 51, 17, 40, 12, 26, 7, 67, 76, 75, 68, 6, 33, 63, 43, 1, 73, 62, 2, 30, 4, 34, 46, 52, 8, 35, 53, 65, 31, 55, 25, 9, 39, 72, 58, 10, 38, 11, 66, 59, 14, 19, 54, 13]
#cut_off_route = [1, 8, 9, 38, 31, 44, 18, 7, 28, 6, 37, 19, 27, 17, 43, 30, 36, 46, 33, 20, 12, 15, 40, 22, 16, 41, 34, 29, 2, 26, 4, 35, 45, 10, 24, 42, 5, 48, 39, 32, 21, 47, 11, 23, 13, 25, 14, 3, 1]
#cut_off_route = [30, 7, 18, 44, 31, 38, 8, 1, 9, 40, 15, 12, 11, 23, 3, 22, 16, 41, 34, 29, 2, 26, 4, 35, 45, 10, 24, 42, 5, 48, 32, 39, 25, 14, 13, 21, 47, 20, 33, 46, 36, 28, 6, 37, 19, 27, 17, 43, 30]
#cut_off_route = [1,8,46,33,20,17,43,27,19,37,6,30,36,28,7,18,44,31,38,9,40,15,12,11,47,21,13,25,14,23,3,22,16,41,34,2,29,5,48,39,32,24,42,10,45,35,4,26,1]
#print routeCost(GD, cut_off_route, 'distance')
#fin = finalisation([cut_off_route, routeCost(GD, cut_off_route, 'distance'), 1, GD], path_finders = 6000, cost_attribute = 'distance')
#route = fin[0]
#print fin[1], fin[4]


# assess the magnitude of task
if nx.number_of_nodes(G) > 20:
  hard = True
else:
  hard = False

#total_time = timeit.timeit('hardTSPsolver(GD, 20000, 1)', number=1)

t = time.clock()
h_path = hardTSPsolver(GD, 10000, 1, 7000)

f_path = finalisation(h_path, 4000)

print 'time', time.clock() - t
print f_path[0], f_path[1], f_path[2], f_path[4]

##for edge in s_path[3].edges(data = True):
  ##if edge[2]['Delaunay']:
    ##print s_path[3][edge[0]][edge[1]]['TSP_weight']['current_weight'], '|', s_path[3][edge[0]][edge[1]]['TSP_weight']['default_weight']

#fraud_route = [1,8,38,31,44,18,7,28,6,37,19,27,17,43,30,36,46,33,20,47,21,32,39,48,5,42,24,10,45,35,4,26,2,29,34,41,16,22,3,23,14,25,13,11,12,15,40,9,1]

route = f_path[0]
showGraph(GD, route, all_edges = None, file_path = '/windows/D/GEO/TSP/symmetrical_TSP/eil76/route_progress/fin.png')
#showGraph(GD, route, label = 'id', all_edges = None)



#f1_path = path + 'eil76_local_optimisation.csv'

#f1_path = path + 'att48_logarithmic_weights.csv'

#f = open(f1_path, 'a')

#GD = createTINgraph(G, to_directed = False)
#for i in xrange(100):
  #try:
    #print 'STEP:', i
    #GD1 = GD.copy()
    #t = time.clock()
    #h_path = hardTSPsolver(GD1, 7000, 1, TSPLIB_mode=True)
    #f_path = finalisation(h_path, 4000, TSPLIB_mode=True)
    #print 'time', time.clock() - t, '|', 'route length:', f_path[1]
    #route = f_path[0]
    #iterations = f_path[2]
    #length = f_path[1]
    #optimisation_gain = f_path[4]
    #test_mode = 'att48 undirected graph'
    #conditions = '"fixed start,\nultimate intersection removal,\nimproved finalisation,\nlogarithmic coefficient,\nrandomised weight"'
    #route = re.sub('[\[\]]', '"', str(route))
    #string = '%s,%s,%d,%d,%s,%d\n' %(test_mode, conditions, length, iterations, route, optimisation_gain)
    #f.write(string)
  #except:
    #pass

#GD = createTINgraph(G, to_directed = True)
#for i in xrange(100):
  #try:
    #print 'STEP:', i
    #GD1 = GD.copy()
    #t = time.clock()
    #h_path = hardTSPsolver(GD1, 3500, 1)
    #f_path = finalisation(h_path, 5000)
    #print 'time', time.clock() - t, '|', 'route length:', f_path[1]
    #route = f_path[0]
    #iterations = f_path[2]
    #length = f_path[1]
    #optimisation_gain = f_path[4]
    #test_mode = 'eil76 directed graph'
    #conditions = '"fixed start,\nultimate intersection removal,\nimproved finalisation,\nimproved weights"'
    #route = re.sub('[\[\]]', '"', str(route))
    #string = '%s,%s,%d,%d,%s,%d\n' %(test_mode, conditions, length, iterations, route, optimisation_gain)
    #f.write(string)
  #except:
    #pass

#f.close()




#path = path + 'a280_SPF_edges_dump.txt'
#f = open(path, 'w')
#for i in GD.edges(data=True):
#  string = str(i) + '\n'
#  f.write(string)
#f.close()

#try:
  #showGraph(GD, s_path[0], 'id')
#except:
  #path = '/windows/D/GEO/TSP/symmetrical_TSP/100_points/fail_routes.txt'
  #f = open(path, 'w')
  #for i in s_path[3]:
    #string = str(i) + '/n'
    #f.write(i)
  #f.close()

#path = '/home/yury/GIS/OMB/docs/'
#f = open(path, 'w')
#for i in xrange(200):
#  print '-----------------%d------------------' %(i)
#  s_path = TSPsolver(GD, 20000, 1)
#  print s_path
#  route = s_path[0]
#  length = s_path[1]
#  iteration = s_path[2]
#  string = '%s, %d, %d\n' %(route, length, iteration)
#  f.write(string)
#
#f.close()


#print GD.edges(data = True)
#print '\n\n\n'
#print GD.nodes(data = True)
#print '\n\n\n'
#print GD.edges(data = True)