# -*- coding: utf-8 -*-
"""
Created on Fri Oct 31 18:11:15 2014

@author: Yury V. Ryabov, riabovvv@gmail.com
"""
from easy_TSP_solver import *
import networkx as nx
import scipy.spatial
import matplotlib.pyplot as plt
import sys
import random
from math import sqrt, log


def createTINgraph(point_graph, id_field='id', show=False, calculate_distance=True, to_directed=False, TSPLIB_mode=False, pseudo_eucledian=False):
  '''
  Creates a graph based on Delauney triangulation

  @param point_graph: either a graph made by read_shp() from another NetworkX's point graph
  @param show: whether or not resulting graph should be shown, boolean
  @param calculate_distance: whether length of edges should be calculated
  @param to_directed: convert graph to directed
  @param TSPLIB_mode: if distances must be integers, boolean
  @param pseudo_eucledian: if the pseudo Eucledian distance should be calculated (for ATT instances of TSPLIB), boolean
  @return - a graph made from a Delauney triangulation

  @Copyright notice: this code is an improved (by Yury V. Ryabov, 2014, riabovvv@gmail.com) version of
                    Tom's code taken from this discussion
                    https://groups.google.com/forum/#!topic/networkx-discuss/D7fMmuzVBAw
  '''

  TIN = scipy.spatial.Delaunay(point_graph)
  edges = set()
  # for each Delaunay triangle
  for n in xrange(TIN.nsimplex):
      # for each edge of the triangle
      # sort the vertices
      # (sorting avoids duplicated edges being added to the set)
      # and add to the edges set
      edge = sorted([TIN.vertices[n,0], TIN.vertices[n,1]])
      edges.add((edge[0], edge[1]))
      edge = sorted([TIN.vertices[n,0], TIN.vertices[n,2]])
      edges.add((edge[0], edge[1]))
      edge = sorted([TIN.vertices[n,1], TIN.vertices[n,2]])
      edges.add((edge[0], edge[1]))


  # make a graph based on the Delaunay triangulation edges
  graph = nx.Graph(list(edges))

  # add nodes attributes to the TIN graph from the original points
  original_nodes = point_graph.nodes(data = True)
  for n in xrange(len(original_nodes)):
    XY = original_nodes[n][0] # X and Y tuple - coordinates of the original points
    graph.node[n]['XY'] = XY
    # add other attributes
    original_attributes = original_nodes[n][1]
    for i in original_attributes.iteritems(): # for tuple i = (key, value)
      graph.node[n][i[0]] = i[1]
  # relabel nodes
  remap = {}
  for node, attribute in graph.nodes(data = True):
    new_name = attribute[id_field]
    remap[node] = new_name
  graph = nx.relabel_nodes(graph, remap)


  # calculate Euclidian length of edges and write it as edges attribute
  if calculate_distance:
    edges = graph.edges()
    for i in xrange(len(edges)):
      edge = edges[i]
      node_1 = edge[0]
      node_2 = edge[1]
      graph.edge[node_1][node_2]['Delaunay'] = True
    # add the rest of edges
    for node in graph.nodes():
      non_neighbors = nx.non_neighbors(graph, node)
      for n in non_neighbors:
        graph.add_edge(node, n)
    edges = graph.edges()
    for i in xrange(len(edges)):
      edge = edges[i]
      node_1 = edge[0]
      node_2 = edge[1]
      x1, y1 = graph.node[node_1]['XY']
      x2, y2 = graph.node[node_2]['XY']
      if not pseudo_eucledian:
        dist = sqrt( pow( (x2 - x1), 2 ) + pow( (y2 - y1), 2 ) )
      else: # Pseudo Euclidian distance (for ATT instances of TSP)
        dist = sqrt( (pow( (x2 - x1), 2 ) + pow( (y2 - y1), 2 ))/10.0 )
        t12 = round(dist)
        if t12 < dist:
          dist = t12 + 1
        else:
          dist = t12
      if TSPLIB_mode:
        dist = round(dist)
      else:
        dist = round(dist, 2)
      graph.edge[node_1][node_2]['distance'] = dist
      try:
        graph.edge[node_1][node_2]['Delaunay']
      except:
        graph.edge[node_1][node_2]['Delaunay'] = False

  # plot graph
  if show:
    showGraph(graph)

  if to_directed: # convert to directed if needed
    graph = graph.to_directed()

  return graph





def iterationCoefficient(iteration):
  '''
  Returns coefficient for weighting based on the iteration the route was acquired

  Parameter
  ---------

  iteration: integer,
      iteration of the route acquirement

  Returns
  -------

  coefficient: float,
      weighting coefficient

  '''
  coefficient = float(iteration)
  coefficient = log(coefficient, 10)*sqrt(coefficient)
  #coefficient = (coefficient / 50) ** 2

  return coefficient


def assignWeights(graph, route, route_cost, steps, iteration, success, duplicated_route=False):
  '''
  Assigns weights to the edges of the graph along the given route depending on whether the route's successfulness

  Parameters
  ----------

  graph: a NetworkX graph

  route: a list of points
      a route along the graph

  route_cost: numerical
      cost of the route

  steps: integer
      number of edges in the route ( steps = len(route) - 1 )

  iteration: integer
      number of

  success: Boolean
      indicates whether route connected all the points from start to end or
      ended up being a longest route

  duplicated_route: Boolean
      indicates that this route already have been explored


  Returns
  -------

  True if executed successfully
  '''
  if isinstance(success, bool):
    pass
  else:
    sys.exit("Parameter 'success' is not Boolean")

  #steps = len(route) - 1 # number of edges in the route (not a parameter 'route')
  #delta_weight = steps / route_cost
  #for step in xrange(steps):
    #node1 = route[step]
    #node2 = route[step + 1]
    #d_step = step + 1
    #step_key = 'step_%s' %(d_step)
    #default_weight = graph[node1][node2]['TSP_weight']['default_weight']
    #try:
      #current_weight = graph[node1][node2]['TSP_weight'][step_key]['current_weight']
    #except:
      #current_weight = default_weight + delta_weight
    #if success and current_weight < default_weight:
      #current_weight = default_weight + delta_weight
      #graph[node1][node2]['TSP_weight'][step_key]['successes'] += 1
    #elif success: # shortest route have been found
      #current_weight += delta_weight
      #graph[node1][node2]['TSP_weight'][step_key]['successes'] += 1
    #else: # a route leads to a deadend
      #current_weight += delta_weight
      #graph[node1][node2]['TSP_weight'][step_key]['dead_ends'] += 1

  #if current_weight < 0: # we don't want either negative or zero weights
    #current_weight = 0.00000001
  #graph[node1][node2]['TSP_weight'][step_key]['current_weight'] = current_weight

  #steps = len(route) - 1 # number of edges in the route (not a parameter 'route')
  coeff = iterationCoefficient(iteration)
  delta_weight = (1 / route_cost) * coeff #(steps / route_cost) * iteration * 1 #100000
  if not success: # makes delta_weight randomly positive or negative
    if duplicated_route:
      delta_weight *= -2.5#-0.5
    #else:
      #multiplyer = random.randrange(-1, 2, 2)
      ##if multiplyer > 0: # increasing negative multiplyer chance
        ##multiplyer = random.randrange(-1, 2, 2)
      #delta_weight *= multiplyer
  else:
    delta_weight *= 5
  for step in xrange(steps):
    node1 = route[step]
    node2 = route[step + 1]
    default_weight = graph[node1][node2]['TSP_weight']['default_weight']
    try:
      current_weight = graph[node1][node2]['TSP_weight']['current_weight']
    except:
      current_weight = default_weight + delta_weight  # shortest route have been found and weight of the edge lower than initial
    if success and current_weight < default_weight:
      current_weight += delta_weight + default_weight
    elif success or duplicated_route: # shortest route have been found
      current_weight += delta_weight
    else: # a route is not the shortest so far
      current_weight += 0 # delta_weight/1000 #* coeff) #delta_weight / (3 * iteration)
      if current_weight < 0:
        current_weight *= -1  # we don't want weights to be negative
    graph[node1][node2]['TSP_weight']['current_weight'] = current_weight
  return True


def chooseNextNode(nodes_dict):
  '''
  Randomly chooses next poont to move taking into account weights of the paths

  Parameters
  ..........

  nodes_dict: dictionary, {node: weight}
          a dictionary that contains nodes as keys; values - dictionary
          with weights for the edge to the current node {node: weight}


  Returns
  .......

  A note to move to.
  '''
  if not nodes_dict: # dictionary is empty
    sys.exit('chooseNextNode(): empty nodes dictionary!')
  # assign probabilities
  total_weight = 0  # sum of weights will be considered as "1" (sum of probabilities)
  weight_dict = {}  # {node: weght}
  weight_difference = {}  # {node: delta_weight}
  to_correct = 0  # number of nodes to correct weights
  total_nodes = len(nodes_dict)  # total number of nodes

  # Proceed randomly
  # Scenario №1: weight based randomised node choice
  # Scenario №2: completely random node choice
  # Scenario №3: highest weight node is chosen
  scenario = random.randrange(0, 10000)/100.0
  if scenario <= 101:
    for weight in nodes_dict.itervalues(): # culculate total weight of nodes
      total_weight += weight
    for node, weight in nodes_dict.iteritems(): # calculate probabilities to choose given node
      new_weight = 100 * float(weight) / total_weight
      #new_weight = round(new_weight, 2)
      weight_dict[node] = new_weight
      if new_weight < 0.0: # if we don't want probabilities to bee too smal change this to the percent treshold (seems to be not usefull)
        weight_difference[node] = 0 - weight
        to_correct += 1
    if to_correct > 0: # recalculate weights
      total_difference = 0
      for value in weight_difference.itervalues():
        total_difference += value
      donation = total_difference
      for node, weight in weight_dict.iteritems():
        sponsor = max(weight_dict.itervalues()) # a value of node to subtract values from
        if weight == sponsor:
          sponsor = node # a node to subtract values from
          break
      weight_dict[sponsor] -= donation
      for node in weight_difference.iterkeys(): # correct probabilities
        delta = weight_difference[node]
        weight_dict[node] += delta
    #choose the next node
    ranges = {} # {(range_start, range_end): node}
    range_start = 0
    range_end = 0
    for node, weight in weight_dict.iteritems():
  #    print node, weight
  #    if range_start == 0:
      range_end = range_start + weight
      ranges[(range_start, range_end)] = node
      range_start = range_end
  #    else:
  #      range_end = range_start + weight
  #      ranges[(range_start, range_end)] = node
  #      range_start += range_end
    chance = float(random.randrange(0, 10001, 1)) / 100
    if chance == 100:
      chance -= 0.01
    for ran in ranges.iterkeys():
      range_start = ran[0]
      range_end = ran[1]
      #if 99 > range_end >= 0.98:
        #range_end = 0.99
      if range_start <= chance <= range_end:
        next_node = ranges[ran]
  #      print 'next node:', next_node
        break
    # unfortunately in rare cases chance is out of range of any given ranges (usually when chance = 0.99). Fix it this way
    try:
      next_node = next_node
    except:
      print 'ahtung! "randes" assignment failed!'
      print 'chance:', chance
      #print ranges
      #for ran in ranges.iterkeys():
        #print 'ran', ran
      lucky = random.randrange(total_nodes)
      i = 0
      for node in ranges.itervalues():
        if i == lucky:
          next_node = node
        else:
          i += 1
  elif scenario > 95: #choose random node to go to
    lucky = random.randrange(total_nodes)
    i = 0
    for node in nodes_dict.iterkeys():
      if i == lucky:
        next_node = node
      else:
        i += 1
  else:  # scenario №3 сhoose the max weight node
    max_weight = max(nodes_dict.itervalues())
    for node, weight in nodes_dict.iteritems():
      if weight == max_weight:
        next_node = node
        break

  return next_node



def easyTSPsolver(graph, path_finders, start_node, end_node = None, cost = 'distance'):
  '''
  Resolves (hopefully ;-) Travelling Salesman Problem at the given graph using 'smart path-finders' algorithm

  Parameters
  ----------

  graph: NetworkX graph
    a graph to resolve TSP with.

  path_finders: integer
    number of 'smart path-finders' to deploy to find the route.

  start_node: key
    a note to start route from.

  end_node: key
    a node to end route, if not specified a starting node is presumed to be end node.

  cost: numeric
    an attribute of the graph edges to derive cost of travel from 'distance' is the default value

  Returns
  -------

  A NetworkX graph which is the shortest path (hopefully ;-) from the start node
  to the end node that visits all other nodes once, lengt of the route
  '''

  graph = graph
  # assign weights to edges
  all_edges = graph.edges()
  for i in xrange(len(all_edges)):
    n1, n2 = all_edges[i]
    default_weight = graph[n1][n2][cost]
    graph[n1][n2]['TSP_weight'] = {'default_weight': None} # will contain weights for each step (step number - key, step number weigth - value)
    # what if user passed some bullshit as a weight???
    try:
      graph[n1][n2]['TSP_weight']['default_weight'] = 1 / default_weight
    except:
      if float(default_weight) == 0.0:
        graph[n1][n2]['TSP_weight']['default_weight'] = 1
      else:
        graph[n1][n2]['TSP_weight']['default_weight'] = 1 / float(default_weight)
    else:
      if not isinstance(default_weight, float) and not isinstance(default_weight, int):
        message = 'ERROR: "%s" attribute of the edge (%d, %d) has type "%s", but it must be either "float" or "integer".\nExecution aborted!' %(cost, n1, n2, type(default_weight))
        sys.exit(message)

  # define end_node
  if not end_node:
    end_node = start_node
    symmetric_TSP = True
  else:
    symmetric_TSP = False



  # start path finding
  if symmetric_TSP:
    total_nodes = len(graph.nodes()) + 1
  else:
    total_nodes = len(graph.nodes())
  shortest_route = None
  shortest_route_length = None
  successful_iteration = None
  iteration = 0
  fail_routes = []
  for i in xrange(path_finders): # launch N path-finders
    iteration += 1
#    print path_finders - i, 'to go'
    nodes_to_go = total_nodes
    end_node_reached = False
    can_move = True
    route = [] # nodes visited during the path-finding
    current_node = start_node
    step = 0 # number of the current step

    while not end_node_reached and can_move:
      step += 1
      step_key = 'step_%d' %(step)
      route.append(current_node)
      nodes_to_go -= 1
      neighbors = graph.neighbors(current_node)
      to_visit = {} # unvisited nodes amongst current neighbors {node: current_weight}
      to_visit_def = {} # unvisited nodes amongst current neighbors {node: default_weight}
      enough_data = True # indicates that there is exist statistics for the current step for each of the egdes to the neighbors

      for i in xrange(len(neighbors)):
        node = neighbors[i]
#        if not symmetric_TSP or (symmetric_TSP and nodes_to_go > 1):
        if node not in route or (symmetric_TSP and nodes_to_go == 1 and node == end_node):
          to_visit_def[node] = graph[current_node][node]['TSP_weight']['default_weight']
          try:
            # add statistics for this edge for current step to the to_visit dict
            to_visit[node] = graph[current_node][node]['TSP_weight'][step_key]['current_weight']
          except:
            # this means that for one or more edges there is no statistis for the current step
            enough_data = False
            to_visit[node] = graph[current_node][node]['TSP_weight']['default_weight']
            # add statistics pattern to edge
            graph[current_node][node]['TSP_weight'][step_key] = {}
            graph[current_node][node]['TSP_weight'][step_key]['successes'] = 0
            graph[current_node][node]['TSP_weight'][step_key]['dead_ends'] = 0
            default_weight = graph[current_node][node]['TSP_weight']['default_weight']
            graph[current_node][node]['TSP_weight'][step_key]['current_weight'] = default_weight
        else:
          continue

      # define conditions for unsuccessful route
      n_ways = len(to_visit)
      if n_ways == 0: # dead end
        can_move = False
        route_length = routeCost(graph, route, cost)
        assignWeights(graph, route, route_length, iteration, False)
      # if route goes on need to choose next node
      else:
        if n_ways == 1: # there is only one node to go
          current_node = to_visit.keys()[0]
          if current_node == end_node and nodes_to_go == 1: # route completed
            end_node_reached = True
            route.append(current_node)
            route_length = routeCost(graph, route, cost)
            if shortest_route_length > route_length or shortest_route_length is None:
              print 'path found!'
              shortest_route_length = route_length
              shortest_route = route
              assignWeights(graph, route, route_length, iteration, True)
              successful_iteration = iteration
            else:
              continue
          elif current_node == end_node and nodes_to_go > 1: # can go only to the end_node, buth some other nodes left to visit
            can_move = False
            route_length = routeCost(graph, route, cost)
            #assignWeights(graph, route, route_length, False)
          else: # route incomplete
            continue
        else:
          # check if there is end_node in canditates for the next move
          if end_node in to_visit.keys():
            del to_visit[end_node]
          if len(to_visit) == 1:
            current_node = to_visit.keys()[0]
          else:
            if enough_data:
              current_node = chooseNextNode(to_visit)
            else:
              current_node = chooseNextNode(to_visit_def)

    #if route not in fail_routes:
      #fail_routes.append(route)
  #for i in fail_routes:
    #print len(i)
  return shortest_route, shortest_route_length, successful_iteration, fail_routes




def hardTSPsolver(graph, path_finders, start_node, iter_gap, end_node=None, cost='distance'):
  '''
  Resolves (hopefully ;-) Travelling Salesman Problem at the given graph using 'smart path-finders' algorithm

  Parameters
  ----------

  graph: NetworkX graph
    a graph to resolve TSP with.

  path_finders: integer
    number of 'smart path-finders' to deploy to find the route.

  start_node: key
    a note to start route from.

  iter_gap: integer
    number of iteration after the last shortest route was found before the algorithm will reset search

  end_node: key
    a node to end route, if not specified a starting node is presumed to be end node.

  cost: key
    an attribute of the graph edges to derive cost of travel from 'distance' is the default value

  Returns
  -------

  A NetworkX graph which is the shortest path (hopefully ;-) from the start node
  to the end node that visits all other nodes once, lengt of the route
  '''

  #graph = graph
  # assign weights to edges
  all_edges = graph.edges()
  for i in xrange(len(all_edges)):
    n1, n2 = all_edges[i]
    default_weight = graph[n1][n2][cost]
    graph[n1][n2]['TSP_weight'] = {'default_weight': None} # will contain weights for each step (step number - key, step number weigth - value)
    # what if user passed some bullshit as a weight???
    try:
       weight = 1 / default_weight
    except:
      if float(default_weight) == 0.0:
        weight = 10
      else:
        weight = 1 / float(default_weight)
    else:
      if not isinstance(default_weight, float) and not isinstance(default_weight, int):
        message = 'ERROR: "%s" attribute of the edge (%d, %d) has type "%s", but it must be either "float" or "integer".\nExecution aborted!' %(cost, n1, n2, type(default_weight))
        sys.exit(message)
    #if graph[n1][n2]['Delaunay']:
      #weight *= 3
    graph[n1][n2]['TSP_weight']['default_weight'] = weight #* 1000
    graph[n1][n2]['TSP_weight']['current_weight'] = weight #* 1000

  # get non-Delaunay edges graph
  delete_edges = []
  for edge in graph.edges(data = True):
    if not edge[2]['Delaunay']:
      delete_edges.append(edge)
  original_graph = graph.copy()
  GDS = graph.copy()
  GDS.remove_edges_from(delete_edges)


  # define end_node
  if not end_node or end_node == start_node:
    end_node = start_node
    symmetric_TSP = True
  else:
    symmetric_TSP = False



  # start path finding
  if symmetric_TSP:
    total_nodes = len(graph.nodes()) + 1
  else:
    total_nodes = len(graph.nodes())
  shortest_route = None
  shortest_route_length = None
  best_routes = {}
  longest_route_length = None
  successful_iteration = 0
  iteration = 0
  delta_iteration = 0
  fail_routes = []
  unique_routes = {}
  steps = 0
  for i in xrange(path_finders): # launch N path-finders
    iteration += 1
    if iteration - successful_iteration > iter_gap: # route founding is stuck so lets refresh the graph
      delta_iteration = iteration - 1
      graph = original_graph.copy()
      try:
        best_routes[shortest_route_length] = [shortest_route, successful_iteration]
      except:
        pass
      shortest_route = None
      shortest_route_length = None
      successful_iteration += iter_gap
    #print path_finders - i, 'to go'
    nodes_to_go = total_nodes
    end_node_reached = False
    can_move = True
    route = [] # nodes visited during the path-finding
    current_node = start_node
    step = 0 # number of the current step

    while not end_node_reached and can_move:
      step += 1
      route.append(current_node)
      nodes_to_go -= 1
      neighbors = graph.neighbors(current_node)
      to_visit = {} # unvisited nodes amongst current neighbors {node: current_weight}
      to_visit_else = {} # unvisited nodes amongst current neighbors {node: default_weight}

      for i in xrange(len(neighbors)):
        node = neighbors[i]
        if node not in route or (symmetric_TSP and nodes_to_go == 1 and node == end_node):
          if graph[current_node][node]['Delaunay']:
            to_visit[node] = graph[current_node][node]['TSP_weight']['current_weight']
          else:
            to_visit_else[node] = graph[current_node][node]['TSP_weight']['current_weight']
        else:
          continue

      # TODO: revise this section: there is some condition missing that creates infinite loop.
      # Maybe it would be better to rework n_ways and n_else_ways and return to the original version of this section
      # define conditions for unsuccessful route
      n_ways = len(to_visit)
      n_else_ways = len(to_visit_else)
      if n_ways == 0:
        n_ways = n_else_ways
        to_visit = to_visit_else

      if n_ways == 0: # dead end (this condition must NEVER trigger)
        print 'dead end happend!'
        can_move = False
        route_length = routeCost(graph, route, cost)
        assignWeights(graph, route, route_length, iteration - delta_iteration, False)
      # if route goes on need to choose next node
      else:
        if n_ways == 1: # there is only one node to go
          current_node = to_visit.keys()[0]
          if current_node == end_node and nodes_to_go == 1: # route completed
            end_node_reached = True
            route.append(current_node)
            opt_route = localOptimisation(graph, route, cost, GDS) # optimised route
            route = opt_route[0]
            route_length = opt_route[1]
            #ri_route = removeIntersection(graph, GDS, route, r_length, cost)
            #route = ri_route[0]
            #route_length = ri_route[1]
            if shortest_route_length > route_length or shortest_route_length is None:
              print 'path found!', iteration, '||', route_length
              shortest_route_length = route_length
              shortest_route = route
              successful_iteration = iteration

              # make a snapshot of the graph
              #directory = '/windows/D/GEO/TSP/symmetrical_TSP/eil76/route_progress/'
              #name = '%d_step.png' %(iteration)
              #string = directory + name
              #showGraph(graph, shortest_route, file_path = string)


              if iteration == 1:  # don't want to assign weights for the very first route found!!!
                steps = len(route) - 1
                longest_route_length = route_length
              else:
                assignWeights(graph, route, route_length, steps, iteration - delta_iteration, True)
            #elif shortest_route_length == route_length:
              #print 'The Same Route!', iteration
            else:
              try:
                unique_routes[str(route)] += 1
                assignWeights(graph, route, route_length, steps, iteration - delta_iteration, False, True)
                #print 'duplicated route with length', route_length, '| count:', unique_routes[str(route)]
              except:
                unique_routes[str(route)] = 1
              #if longest_route_length < route_length:
                #print 'longest route found!', iteration
              #longest_route_length = route_length
                assignWeights(graph, route, route_length, steps, iteration - delta_iteration, False, False)
              #else:
                #pass
            #if symmetric_TSP:
              #lucky = random.randrange(total_nodes)
              #start_node = route[lucky]
              #end_node = start_node

            # find out how many unique routes do we hane


          # TODO - is this even possible???
          elif current_node == end_node and nodes_to_go > 1: # can go only to the end_node, buth some other nodes left to visit
            print '"elif current_node == end_node and nodes_to_go > 1" did happen!', iteration
            can_move = False
            route_length = routeCost(graph, route, cost)
            assignWeights(graph, route, route_length, steps, iteration - delta_iteration, False)
          else: # route incomplete
            continue
        else:
          # check if there is end_node in canditates for the next move
          if end_node in to_visit.keys():
            del to_visit[end_node]
          if len(to_visit) == 1:
            current_node = to_visit.keys()[0]
          else:
            current_node = chooseNextNode(to_visit)



      #print nodes_to_go, current_node
    #if route not in fail_routes:
      #fail_routes.append(route)
  #for i in fail_routes:
    #print len(i)
  if len(best_routes) > 0:
    best_routes[shortest_route_length] = [shortest_route, successful_iteration]
    #print best_routes
    shortest_route_length = min(best_routes.iterkeys())
    shortest_route = best_routes[shortest_route_length][0]
    successful_iteration = best_routes[shortest_route_length][1]
    #print shortest_route_length
    #print shortest_route

  print 'unique routes:', len(unique_routes)

  return shortest_route, shortest_route_length, successful_iteration, graph



def localOptimisation(graph, route, cost, d_graph=None):
  '''
  Optimises given route to minimise its length

  Parameters
  ----------

  graph: a NetworkX graph

  d_graph: a networkx graph,
    graph with non-Delaunay edges removed

  route: list,
      a sequence of points in the graph

  cost: key
    an attribute of the graph edges to derive cost of travel from 'distance' is the default value

  Returns
  -------

  route: list,
      a sequence of points in the graph

  cost: numeric,
      cost of te optimised_route

  '''
  old_length = routeCost(graph, route, cost)
  optimised_route = route
  if d_graph:
    no_intersections = removeIntersection(graph, d_graph, route, old_length, cost)
    optimised_route = no_intersections[0]
    old_length = no_intersections[1]


  #last_step_success = False # indicates whether at the last step local optimisation was successfull
  #last_step_optimisation = None
  for i in xrange(len(route) - 3):
    old_route = [] # local route
    for a in xrange(4):
      old_route.append(optimised_route[i+a])
    old_route_cost = routeCost(graph, old_route, cost)
    new_route = [old_route[0], old_route[2], old_route[1], old_route[3]]
    new_route_cost = routeCost(graph, new_route, cost)
    #print 'old:', old_route
    #print 'new:', new_route
    #print optimised_route
    optimisation_result = new_route_cost - old_route_cost
    if optimisation_result < 0:
      #print 'local success'
      #print 'local cost difference:', new_route_cost - old_route_cost
      #print optimised_route[i + 1], new_route[2]
      #print optimised_route[i + 2], new_route[1]
      #if not last_step_success or (last_step_success and last_step_optimisation < optimisation_result):
      optimised_route[i + 1] = new_route[1]
      optimised_route[i + 2] = new_route[2]
      #last_step_success = True
      #last_step_optimisation = optimisation_result
      #else:
        #last_step_success = False
        #last_step_optimisation = None
      #print 'after optimisation:', optimised_route
    #else:
      #last_step_success = False
      #last_step_optimisation = None
  #print 'initial cost:', route
  #print 'optimised cost', optimised_route
  if len(route) != len (optimised_route):
    #print optimised_route
    #print route
    sys.exit('localOptimisation(): route optimisation failed T_T')
  new_length = routeCost(graph, optimised_route, cost)
  # check if there are mistakes
  i = 0
  for node in route:
    i += 1
    if node not in optimised_route:
      print 'invalid route optimisation!', node, i
      print route
      print optimised_route
      optimised_route = route
      break
  if new_length > old_length: # local optimisation failed
    optimised_route = route
    new_length = old_length
  #if old_length != new_length:
    #print 'optimisation:', routeCost(graph, route, cost), routeCost(graph, route, cost) - routeCost(graph, optimised_route, cost)
  return optimised_route, new_length




def finalisation(TSP_output, path_finders = 10000, cost_attribute = 'distance', TSP = True):
  '''
  Given output from hardTSPsolver() optimises the route on local path of 20 points

  Parameters
  ----------

  TSP_output: tuple,
        output of the hardTSPsolver(): (shortest_route, shortest_route_length, successful_iteration, graph).

  path_finders: integer
    number of 'smart path-finders' to deploy to find the route.

  cost_attribute: string,
        name of the edge attribute that contains cost information.

  TSP: boolean,
      indicates whether TSP problem is solved or not

  Returns
  -------

  finalised route of found by hardTSPsolver() and additional data (route, finalised_length, successful_iteration, graph)
  '''
  graph = TSP_output[3]
  route = TSP_output[0]
  hard_TSP_length = TSP_output[1]
  successful_iteration = TSP_output[2]

  # delete non-Delaunay edges
  delete_edges = []
  for edge in graph.edges(data = True):
    if not edge[2]['Delaunay']:
      delete_edges.append(edge)
  GDS = graph.copy()
  GDS.remove_edges_from(delete_edges)
  # get non-Delaunay route parts
  cut_offs = [] # non-Delaunay route parts
  for i in xrange( (len(route) - 1) ):
    node1 = route[i]
    node2 = route[i + 1]
    if not graph.edge[node1][node2]['Delaunay']:
      #print 'non Delaunay edge detected!'
      revork_list = [node1, node2]
      neighbors1 = GDS.neighbors(node1)
      neighbors2 = GDS.neighbors(node2)
      for i in neighbors1:
        if i in neighbors2:
          revork_list.append(i)
      #print len(revork_list)
      #if len(revork_list) >= 4:
      cut_offs.append(revork_list)
      #else:
        #print 'difficult cut off:', revork_list
  # try to reorder route to eliminate cut offs
  delau_route = route
  delau_route_length = hard_TSP_length
  print 'cut_offs', cut_offs
  for node_list in cut_offs:
    if len(node_list) != 4: # reduce to 4 points in list
      #node_list = node_list[:3] # TODO: find if the segments are crossing each other http://algolist.manual.ru/maths/geom/intersect/lineline2d.php
      segment = node_list[:2]
      points_list = node_list[2:]
      new_node_list = findIntersection(segment, points_list, delau_route, graph)
      print 'new_node_list:', new_node_list
      if len(new_node_list) == 4:
        node_list = new_node_list
      else:
        print 'intersection detection failed'
    #elif len(node_list) < 4: # TDO: find out how to handle cut offs of this type
      #pass

    #print 'node_list:', node_list
    list1 = []
    list2 = []
    list3 = []
    all_lists = [list1, list2, list3]
    p = 0
    current_list = all_lists[p]
    for node in delau_route:
      current_list.append(node)
      if node in node_list and p < 2 and not (p == 1 and len(current_list) == 1): # list2 must not consist of only 1 point
        p += 1
        current_list = all_lists[p]
    # to get rid of cut off we just need to reverse list2 order and combine 3 lists together
    r_list2 = reversed(list2)

    #print 'list1:', list1
    #print 'list2:', list2
    #print 'list3:', list3

    new_list2 = []
    for node in r_list2:
      new_list2.append(node)
    #print 'new_list2:', new_list2
    new_delau_route = list1 + new_list2 + list3
    #print 'new_delau_route:', new_delau_route
    new_delau_route_length = routeCost(graph, new_delau_route, cost_attribute)
    if new_delau_route_length <= delau_route_length:
      print 'delau success'
      delau_route = new_delau_route
      delau_route_length = new_delau_route_length
    else:
      try:
        l1 = list1
        l2 = list3[:-1]
        l3 = new_list2
        l4 = [list3[-1]]
        #new_delau_route = list1 + list3[:-1]+new_list2 + [list3[-1]]
        new_delau_route = l1 + l2 + l3 + l4
        #print 'else:', new_delau_route
        new_delau_route_length = routeCost(graph, new_delau_route, cost_attribute)
        if new_delau_route_length <= delau_route_length:
          print 'delau success'
          delau_route = new_delau_route
          delau_route_length = new_delau_route_length
        else:
          print 'new_delau_route_length failed'
      except:
        print 'new_delau_route: CRITICAL FAILURE!'
        print list1, list2, list3
  route = delau_route
  #print 'delauney done!', optimised_route


  checked_start = False # indicates whether finalisation around starting node was made
  i = 0
  while i <= (len(route)-20):
    print checked_start
    if checked_start:
      local_route = route[i: i+20]
    else:
      part1 = route[-5:]
      part2 = route[1:6]
      local_route = part1 + part2
    print len(local_route)
    if len(local_route) != 1: # TODO there is some strange thing happening sometimes when local route consists only of one point duno why (=_=)
      try:
        local_route_length = routeCost(graph, local_route, cost_attribute)
        GDSL = GDS.subgraph(local_route)
        start_point = local_route[0]
        end_point = local_route[-1]
        l_path = TSPsolver(GDSL, path_finders, start_point, end_point)
        if l_path[1] is not None:
          #print l_path
          l_path_length = l_path[1]
          if l_path_length < local_route_length:
            print 'local optimisation success!'
            if checked_start:
              for n in xrange(len(local_route)):
                route[i + n] = l_path[0][n]
            else:
              for j in xrange(-5, 0):
                route[j] = l_path[0][j+5]
              for k in xrange(0, 6):
                route[k] = l_path[0][k+4]
      except:
        checked_start = True
        i+=1
        pass
    if checked_start:
      i += 3
    else:
      checked_start = True
  # calculate finalised route length
  #finalised_length = routeCost(graph, route, cost_attribute)
  #if finalised_length > hard_TSP_length:
    #finalised_length = hard_TSP_length

  lo = localOptimisation(graph, route, cost_attribute)
  route = lo[0]
  finalised_length = lo[1]
  if TSP and route[0] != route[-1]:
    print 'CRITICAL FINALISATION FALURE!!!', route
    route = TSP_output[0]
    finalised_length = TSP_output[1]

  return route, finalised_length, successful_iteration, graph, hard_TSP_length -finalised_length


def removeIntersection(graph, d_graph, route, route_length, cost_attribute):
  '''
  Removes most of the self-intersections from the route

  Parameters
  ----------

  graph: a networkx graph

  d_graph: a networkx graph,
      graph with non-Delaunay edges removed

  route: list,
      list of nodes that forms route on the graph

  route_length: numerical,
      length of the route

  cost_attribute: string,
      name of the edge attribute of the graph that stores cost data

  Returns
  -------

  route: list,
      list of points of the graph where most of the self-intersections removed

  route_length: numerical,
      length of the rote with removed self-intersections


  '''
  GDS = d_graph.copy() #TODO get rid of 'GDS' - replace it by 'd_graph'

  success = True
  while success == True: # eliminate self intersection while possible
    success = False
    #print success
    # get non-Delaunay route parts
    cut_offs = [] # non-Delaunay route parts
    for i in xrange( (len(route) - 1) ):
      node1 = route[i]
      node2 = route[i + 1]
      if not graph.edge[node1][node2]['Delaunay']:
        #print 'non Delaunay edge detected!'
        revork_list = [node1, node2]
        neighbors1 = GDS.neighbors(node1)
        neighbors2 = GDS.neighbors(node2)
        for i in neighbors1:
          if i in neighbors2:
            revork_list.append(i)
        #print len(revork_list)
        #if len(revork_list) >= 4:
        cut_offs.append(revork_list)
        #else:
          #print 'difficult cut off:', revork_list
    # try to reorder route to eliminate cut offs
    delau_route = route
    delau_route_length = route_length
    #print 'cut_offs', cut_offs
    for node_list in cut_offs:
      if len(node_list) != 4: # reduce to 4 points in list
        segment = node_list[:2]
        points_list = node_list[2:]
        new_node_list = findIntersection(segment, points_list, delau_route, graph)
        #print 'new_node_list:', new_node_list
        if len(new_node_list) == 4:
          node_list = new_node_list
        else:
          #print 'intersection detection failed'
          pass
      #elif len(node_list) < 4: # TDO: find out how to handle cut offs of this type
        #pass

      #print 'node_list:', node_list
      if len(node_list) == 4:
        list1 = []
        list2 = []
        list3 = []
        all_lists = [list1, list2, list3]
        p = 0
        current_list = all_lists[p]
        for node in delau_route:
          current_list.append(node)
          if node in node_list and p < 2 and not (p == 1 and len(current_list) == 1): # list2 must not consist of only 1 point
            p += 1
            current_list = all_lists[p]
        # to get rid of cut off we just need to reverse list2 order and combine 3 lists together
        r_list2 = reversed(list2)

        #print 'list1:', list1
        #print 'list2:', list2
        #print 'list3:', list3

        new_list2 = []
        for node in r_list2:
          new_list2.append(node)
        #print 'new_list2:', new_list2
        new_delau_route = list1 + new_list2 + list3
        #print 'new_delau_route:', new_delau_route
        new_delau_route_length = routeCost(graph, new_delau_route, cost_attribute)
        if new_delau_route_length < delau_route_length:
          #print 'delau success'
          success = True
          delau_route = new_delau_route
          route_length = new_delau_route_length
        else:
          try:
            l1 = list1
            l2 = list3[:-1]
            l3 = new_list2
            l4 = [list3[-1]]
            #new_delau_route = list1 + list3[:-1]+new_list2 + [list3[-1]]
            new_delau_route = l1 + l2 + l3 + l4
            #print 'else:', new_delau_route
            new_delau_route_length = routeCost(graph, new_delau_route, cost_attribute)
            if new_delau_route_length < delau_route_length:
              #print 'delau success'
              success = True
              delau_route = new_delau_route
              route_length = new_delau_route_length
            else:
              #print 'new_delau_route_length failed'
              pass
          except:
            print 'new_delau_route: CRITICAL FAILURE!'
            print list1, list2, list3
    route = delau_route
  return route, delau_route_length


def findIntersection(segment, points_list, route, graph):
  '''
  Finds out which of the route segment is intersected by the given segment

  Parameters
  ----------

  segment: list (tuple) of two,
      non-Delaunay part of the route wich is supposed to intersect some other
      segment of the route

  points_list: list
      (unordered) list of point (their IDs in the networkx graph)

  route: list
      (ordered) list of points that forms a route on the networkx graph

  graph: networkx graph

  Returns
  -------

  intersection: list
      (ordered) list of two points that are the part of the route on the networkx graph
  '''
  intersection = []
  for node in segment:
    intersection.append(node)

  # form possible intersection segments
  line1 = makeLine(segment, graph)
  test_segments = []
  if len(points_list) > 1:
    for node in points_list:
      for i in xrange(len(points_list)):
        other_node = points_list[i]
        if node != other_node:
          test_segments.append( [node, other_node] )
    for pair in test_segments:
      node1 = pair[0]
      node2 = pair[1]
      if graph.edge[node1][node2]['Delaunay']:
        line2 = makeLine(pair, graph)
        if isCrossing(line1, line2):
          for node in pair:
            intersection.append(node)
          break
  else:
    for i in xrange( len(route) - 1 ):
      node = route[i]
      other_node = route[i + 1]
      if node not in segment and other_node not in segment:
        test_segments.append( [node, other_node] )
    for pair in test_segments:
      node1 = pair[0]
      node2 = pair[1]
      line2 = makeLine(pair, graph)
      if isCrossing(line1, line2):
        for node in pair:
          intersection.append(node)
        break

  return intersection



def makeLine(segment, graph, XY_attribute = 'XY'):
  '''
  Creates a list of tuples - coordinates of the nodes to form a lineline2d

  Parameters
  ----------

  segment: list,
      list of nodes in networkx graph

  graph: networkx graph

  XY_attribute: string,
      attribute of the nodes in networkx graph where XY coordinates are stored

  Returns
  -------

  line: list,
      list of tuples like that: [(Xi, Yi),(Xj, Yj)]
  '''
  line = []
  for node in segment:
    XY = graph.node[node][XY_attribute]
    line.append(XY)

  return line



def isCrossing(segment1, segment2):
  '''
  Checcks if segments have intersections
  Algrithm described here: http://algolist.manual.ru/maths/geom/intersect/lineline2d.php

  Parameters
  ----------

  segment1: list of 2 tuples [(Xi1, Yi1),(Xi2, Yi2)],
        a segment of the route defined by XY coordinates

  segment2: list of 2 tuples [(Xj1, Yj1),(Xj2, Yj2)],
      a segment of the route

  Returns
  -------

  result: boolean
      True if intersection exists, False if there is no intersection
  '''
  # get coordinates of points
  point1a = segment1[0]
  point1b = segment1[1]
  point2a = segment2[0]
  point2b = segment2[1]
  X1a = point1a[0]
  Y1a = point1a[1]
  X1b = point1b[0]
  Y1b = point1b[1]
  X2a = point2a[0]
  Y2a = point2a[1]
  X2b = point2b[0]
  Y2b = point2b[1]

  denominator = (Y2b - Y2a)*(X1b - X1a) - (X2b - X2a)*(Y1b - Y1a)
  numerator1 = (X2b - X2a)*(Y1a - Y2a) - (Y2b - Y2a)*(X1a - X2a)
  numerator2 = (X1b - X1a)*(Y1a - Y2a) - (Y1b - Y1a)*(X1a - X2a)
  try: # possible zero division
    cross1 = numerator1 / denominator
    cross2 = numerator2 / denominator
    if (0 <= cross1 <= 1) and (0 <= cross2 <= 1):
      result = True
    else:
      result = False
  except:
    if numerator1 == 0 and numerator2 == 0:
      result = False
    else:
      result = False

  return result