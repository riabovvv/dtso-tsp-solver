This is the Python (2.x) implementation for the Delaunay Triangulation Smart
 Optimisation (DTSO) algorithm for solving Travelling Salesman Problem (TCP)
  and Messenger Problem.
  
DTSO algorithm is described in [article by Yury Ryabov][1] and 
[presentation][2].


Code was written in 2014 and needs refactoring.

[1]: https://elibrary.ru/item.asp?id=26573851
[2]: http://gisconf.ru/pres/1_22_1700_ryabov.pdf
